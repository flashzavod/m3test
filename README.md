#Калькулятор

простая библиотека, которая:
- Получает на вход строку, в которой цифры и знаки математических действий.
- Выдает на выходе результат или бросает ошибка.

Описание структуры:

src - исходники библиотеки
tests - папка с тестами
src\strategy - стратеги подсчета результата
src\validator - стратегия валидации входящей строки

Пример вызова:

```php
$strategy = MathConfig::strategy();

$context = new MathContext($strategy);

$context->execute('4+4+1');
```

Запуск тестов локально:

```
./vendor/bin/phpunit --bootstrap vendor/autoload.php tests/MathTest
```

Запуск тестов в Docker:
```
docker build -t app . && docker run -it app ./vendor/bin/phpunit --bootstrap vendor/autoload.php tests/MathTest
  ```