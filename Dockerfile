FROM cespi/php-5.3:modules-fpm-latest

WORKDIR /var/www/html

COPY . ./

#install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin/ --filename=composer

RUN composer install

CMD ["composer"]