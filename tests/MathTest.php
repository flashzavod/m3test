<?php

use M3Test\MathContext;
use M3Test\MathConfig;

require 'vendor/autoload.php';

class MathTest extends PHPUnit_Framework_TestCase
{

    public function testCalcString()
    {
        $strategy = MathConfig::strategy();

        $context = new MathContext($strategy);

        $this->assertEquals(9, $context->execute('4+4+1'));

        $this->assertEquals(4, $context->execute('16/2/2'));

        $this->assertEquals(5, $context->execute('7-1-1'));

        $this->assertEquals(12, $context->execute('3*2*2'));
    }

    public function testException()
    {
        $strategy = MathConfig::strategy();

        $context = new MathContext($strategy);

        $this->setExpectedException('Exception');

        $context->execute('wrong string');
    }
}