<?php

namespace M3Test;

use M3Test\Strategy\NativeStrategy;
use M3Test\Validator\NativeValidator;

class MathConfig
{
    public static function strategy()
    {
        return new NativeStrategy;
    }

    public static function validator()
    {
        return new NativeValidator;
    }
}