<?php

namespace M3Test;

use M3Test\Strategy\MathStrategy;

class MathContext
{
    private $namingStrategy;

    function __construct(MathStrategy $strategy)
    {
        $this->namingStrategy = $strategy;
    }

    function execute($query)
    {
        if (MathConfig::validator()->validate($query) == false) {
            throw new \Exception('Query is wrong');
        }

        return $this->namingStrategy->calc($query);
    }
}