<?php

namespace M3Test\Strategy;

interface MathStrategy
{
    /**
     * @param string $query
     * @return double|integer
     */
    public function calc($query);
}