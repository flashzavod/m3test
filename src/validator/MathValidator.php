<?php

namespace M3Test\Validator;

interface MathValidator
{

    /**
     * @param string $query
     * @return boolean
     */
    public function validate($query);
}