<?php

namespace M3Test\Validator;

class NativeValidator implements MathValidator
{
    public function validate($query)
    {
        return (bool) preg_match("/(\d|\+|\-|\/|\*)+/i", $query);
    }
}